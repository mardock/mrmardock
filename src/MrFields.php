<?php

namespace MrMardock;

use Exception;
use MrMardock\MrBase;

class MrFields 
{
    private $inputs = [
        'text',
        'number',
        'file',
        'date',
    ];

    public function __call($method, $args)
    {
        if( in_array($method, $this->inputs) ){
            dd($method);
        }
    }

}