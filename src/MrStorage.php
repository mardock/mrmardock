<?php

namespace MrMardock;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MrStorage {
    
    /**
     * Storage Easy.
     *
     * @param  string $property  filename in request
     * @param  string $path      ubicacion donde se guardara /public/storage/$path example /folder/
     * @param  string $name      file name
     * @param  string $disk      driver of laravel public/local
     * @return Object
     */
    public static function storage(string $property, string $path, string $name = null, $disk = 'local'){
        try {
            if(request()->hasFile($property)){ //Existe el archivo en el request ¿?
                
                $f = request()->file($property);
                $ext = strtolower($f->getClientOriginalExtension()); //obtenemos la extension del archivo
                if($name==null){
                    $name = $f->getClientOriginalName();
                    $exp = explode('.'.$ext,$name);
                    $name = count($exp) == 2 ? $exp[0] : $name;
                } 
                $ext = $f->getClientOriginalExtension(); //obtenemos la extension del archivo
                
                $parts = explode('/',$path); //partimos el path
                
                $transforming = null;
                foreach($parts as $p){
                    if($p != "" || $p != null){
                        $transforming .= Str::slug($p,'_').'/'; //converting whitespace into _
                    }
                }
                $path = $transforming;
                
                $pathFile = $path . $name.'.'.$ext; //formateamos el path
                
                $storage = Storage::disk($disk)->put($pathFile, File::get($f)); //guardamos
                
                $pathFull = $disk == 'local' ? $pathFile : Storage::disk('public')->url($pathFile);;

                return (object)['status' => 'OK' , 'path' => $pathFull, 'file_name'=>$name];   //retornamos
            }else{
                return (object)['status' => 'ERROR', 'path' => null];
            }
        } catch (\Exception $e) 
        {
            return (object)['status' => 'ERROR', 'path' => null, 'message' => $e->getMessage()];
        }
    }
    public static function storage64(string $base64, string $path, string $name, string $extension, string $toConvert = null , $disk = 'local' ){
        try {
            if($base64){
                
                $f = base64_decode($base64);               
                $f = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'. $f;
                
                $temp = tmpfile();
                fwrite($temp, $f); //creamos un archivo temporal
                
                // Get the path of the temp file.
                $tempPath = stream_get_meta_data($temp)['uri']; //obtenemos la ruta
                
                $xml = simplexml_load_file($tempPath); //el svg lo transformamos a xml
                $attr = $xml->attributes(); //obtenemos los atributos width y height
                
                // Usamos UploadedFile.
                $imageName = uniqid().time(). $toConvert??".png"; //creamos un nombre "temporal"
                $file = new \Illuminate\Http\UploadedFile($tempPath, $imageName, null, null, true);

                $image = new \Imagick();

                $image->readImageBlob($f);
                $image->setImageFormat($toConvert??$extension);
                $image->setImageExtent((int)$attr->width, (int)$attr->height);

                $ext = $toConvert??$extension;
                
                $parts = explode('/',$path); //partimos el path
                
                $transforming = null;

                foreach($parts as $p){
                    if($p != "" || $p != null){
                        $transforming .= Str::slug($p,'_').'/'; //converting whitespace into _
                    }
                
                }
                $path = $transforming;
                
                $pathFile = $path . $name.'.'.$ext; //formateamos el path
                
                $storage = Storage::disk($disk)->put($pathFile, $image->getImageBlob()); //guardamos
                
                $pathFull = $disk == 'local' ? $pathFile : Storage::disk('public')->url($pathFile);;

                return (object)['status' => 'OK' , 'path' => $pathFull, 'file_name'=>$name];   //retornamos
            }else{
                return (object)['status' => 'ERROR', 'path' => null];
            }
        } catch (\Exception $e) 
        {
            dd($e->getMessage());
        }
    }
    public static function getMIME($extension){
        $data = [
            'jpeg'  =>  'image/jpeg',
            'jpg'   =>  'image/jpeg',
            'png'   =>  'image/png',
            'svg'   =>  'image/svg+xml',
            'pdf'   =>  'application/pdf',
            'ppt'   =>  'application/vnd.ms-powerpoint',
            'pptx'  =>  'application/vnd.ms-powerpoint', //
            'mp4'   =>  'application/mp4',
            'mp4'   =>  'application/mp4',
            'xls'   =>  'application/vnd.ms-excel',
            'doc'   =>  'application/msword',
            'docx'  =>  'application/msword', //d
        ];
        return array_key_exists($extension,$data) ? $data[$extension] : 'media-type';
    }
}

