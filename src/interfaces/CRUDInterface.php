<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

interface CRUDInterface {

    public function index();

    public function create(): View;

    public function storage(Request $request);

    public function edit(Request $request): View;

    public function update(Request $request);

}