<?php

namespace MrMardock;

use Exception;

class MrBase {

    // Fillable for Model
    protected static $namespace = '\\App\\Models\\';
    
    public static function getModel(String $model)
    {
        try{

            return self::$namespace.$model;
            
        }catch(Exception $e){
            dd('error:'. $e->getMessage());
        }
    }
}